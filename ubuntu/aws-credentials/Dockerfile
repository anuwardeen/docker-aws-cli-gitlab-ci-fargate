FROM ubuntu:20.04

# ---------------------------------------------------------------------------- #
# Install the AWS CLI version 2.                                               #
# ---------------------------------------------------------------------------- #
RUN apt-get update -qq -y \
    && apt-get install -qq -y curl unzip \
    && curl -Lo awscliv2.zip https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip \
    && unzip awscliv2.zip \
    && ./aws/install \
    && rm -rf awscliv2.zip ./aws 

# ---------------------------------------------------------------------------- #
# Install and configure 'jq', a tool to parse Amazon ECS temporary/unique      #
# security credentials, which come in the JSON format.                         #
# ---------------------------------------------------------------------------- #
RUN apt-get install -qq -y jq

# ---------------------------------------------------------------------------- #
# Install and configure sshd.                                                  #
# https://docs.docker.com/engine/examples/running_ssh_service for reference.   #
# ---------------------------------------------------------------------------- #
RUN apt-get install -qq -y openssh-server \
    && mkdir -p /var/run/sshd

EXPOSE 22

# ---------------------------------------------------------------------------- #
# Install GitLab CI required dependencies.                                     #
# ---------------------------------------------------------------------------- #                 
ARG GITLAB_RUNNER_VERSION=v12.9.0

RUN curl -Lo /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/${GITLAB_RUNNER_VERSION}/binaries/gitlab-runner-linux-amd64 \
    && chmod +x /usr/local/bin/gitlab-runner

RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash \
    && apt-get install -qq -y git-lfs \
    && git lfs install --skip-repo

# ---------------------------------------------------------------------------- #
# Install https://github.com/krallin/tini - a very small 'init' process        #
# that helps processing signalls sent to the container properly.               #
# ---------------------------------------------------------------------------- #
ARG TINI_VERSION=v0.19.0

RUN curl -Lo /usr/local/bin/tini https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-amd64 \
    && chmod +x /usr/local/bin/tini

# ---------------------------------------------------------------------------- #
# Execute a startup script.                                                    #
# https://success.docker.com/article/use-a-script-to-initialize-stateful-container-data
# for reference.                                                               #
# ---------------------------------------------------------------------------- #
COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["tini", "--", "/usr/local/bin/docker-entrypoint.sh"]
