#!/bin/sh

printTestHeader() {
    echo "$1"
    echo '----------'
}

printTestFooter() {
    echo '----------'
    echo 'Done!'
    echo
}

echo

# ---------------------------------------------------------------------------- #
# Verify environment variables containing `AWS_` are set.                      #
# Please add the `-e AWS_TEST=aws-value` option when starting a container to   #
# run this test.                                                               #
# ---------------------------------------------------------------------------- #
printTestHeader 'Testing whether variables containing `AWS_` are set...'

EXPECTED_AWS_VAR_EXPORT_CMD='export AWS_TEST=aws-value'
echo "expected command: ${EXPECTED_AWS_VAR_EXPORT_CMD}"

AWS_VAR_EXPORT_CMD=$(cat $1 | grep AWS_)
[ "$AWS_VAR_EXPORT_CMD" = "$EXPECTED_AWS_VAR_EXPORT_CMD" ] || exit 1
printTestFooter

# ---------------------------------------------------------------------------- #
# Verify environment variables containing `ECS_` are set.                      #
# Please add the `-e ECS_TEST=ecs-value` option when starting a container to   #
# run this test.                                                               #
# ---------------------------------------------------------------------------- #
printTestHeader 'Testing whether variables containing `ECS_` are set...'

EXPECTED_ECS_VAR_EXPORT_CMD='export ECS_TEST=ecs-value'
echo "expected command: ${EXPECTED_ECS_VAR_EXPORT_CMD}"

ECS_VAR_EXPORT_CMD=$(cat $1 | grep ECS_)
[ "$ECS_VAR_EXPORT_CMD" = "${EXPECTED_ECS_VAR_EXPORT_CMD}" ] || exit 1
printTestFooter
